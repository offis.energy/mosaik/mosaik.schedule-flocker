# mosaik.Schedule-Flocker

Create flocks of schedules for electricity units described by a set of
parameters. These  example schedules can capture the flexibility of a 
model electricity unit.

## Status

[![pipeline status](https://gitlab.com/offis.energy/mosaik/mosaik.schedule-flocker/badges/master/pipeline.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.schedule-flocker/pipelines)
[![coverage report](https://gitlab.com/offis.energy/mosaik/mosaik.schedule-flocker/badges/master/coverage.svg)](https://gitlab.com/offis.energy/mosaik/mosaik.schedule-flocker/-/jobs)

## Maturity

This simulator was developed for the Designetz project, but never used there.

##  Compatibility

This mosaik component uses the stack of SemVer forks instead of the upstream 
versions of SimPy, mosaik, and its API components.

##  Installing system-level prerequisites

-   Python >= 3.7

##  Creating the virtual environment

    python3.8 -m venv venv

##  Installing venv-level requirements

    venv/bin/python -m pip install -r requirements.d/venv.txt

##  Setting up the runtime environment

    venv/bin/python -m tox -e py38 --notest

##  Executing the tests

    venv/bin/python -m tox -e py38

##  Upgrading the requirements

    venv/bin/python -m pur -r requirements.d/venv.txt

    venv/bin/python -m pur -r requirements.d/base.txt

## Freezing the requirements

    venv/bin/python -m pip freeze --all --exclude-editable | \
        grep -v "pkg-resources" > requirements.d/venv.txt

    .tox/py38/bin/python -m pip freeze --all --exclude-editable | \
        grep -v "pkg-resources" > requirements.d/base.txt
