from pathlib import Path

import pytest

from mosaik_schedule_flocker.schedule_flocker.util.paths import ROOT_PATH
from mosaik_schedule_flocker.schedule_flocker.util import paths


@pytest.fixture(name='root_path')
def root_path_fixture():
    return ROOT_PATH


def test_root_path_type(root_path):
    assert isinstance(root_path, Path)


def test_root_path_as_path_object(root_path):
    assert root_path == Path(__file__).parent.parent.parent


def test_root_path_as_string_object(root_path):
    assert str(root_path) == str(Path(__file__).parent.parent.parent)


def test_paths_are_strings_or_paths():
    global_paths = [item for item in dir(paths) if not item.startswith("__")]
    for member in global_paths:
        assert type(member) in (str, Path)
