from mosaik_schedule_flocker.schedule_flocker.core.generator import Generator
from mosaik_schedule_flocker.schedule_flocker.core.util import SCHEDULE_LENGTH
from mosaik_schedule_flocker.schedule_flocker.data.\
    get_mock_flocker_config_module import get_mock_flocker_config
from mosaik_schedule_flocker.schedule_flocker.model.electricity_schedule \
    import ElectricitySchedule


def test_generate_schedule_flock_by_objects():
    # Mock
    # start_date = datetime.strptime(
    #     "2018-05-08T13:43:00+0100",
    #     "%Y-%m-%dT%H:%M:%S%z")
    predicted_schedule = list([1, 2, 6, 2, 3, 5, 8, 2, 3, 6, 2, 9, 2, 7, 3, 7, 5, 7, 3, 8, 1, 7, 5, 6])
    # predicted_flexibility_potential_negative = list(
    #    [2, 6, 2, 8, 5, 7, 3, 6, 3, 6, 7, 2, 8, 2, 8, 7, 5, 1, 2, 4, 3, 6, 8, 9])
    # predicted_flexibility_potential_positive = list(
    #     [5, 2, 7, 2, 4, 1, 7, 3, 5, 9, 3, 5, 2, 8, 9, 3, 1, 5, 2, 5, 2, 7, 9, 3])
    # limit_power_production = 42
    # limit_power_consumption = -42
    limit_energy_production = 20
    limit_energy_consumption = -20
    limit_count_activation = 3
    # limit_size_flock = 10
    # limit_time_generation = 5

    config = get_mock_flocker_config(0)

    generator = Generator()

    for _ in range(100):

        # Test
        schedule_flock = generator.generate_schedule_flock_by_objects(
            config=config)

        # Assert
        for electricity_schedule in schedule_flock:
            print(f'schedule {electricity_schedule}')
            assert type(electricity_schedule) is ElectricitySchedule
            assert len(electricity_schedule) == 24

            # for further tests, take values from schedule
            schedule = electricity_schedule.electricity_values_as_list()
            print(
                str(
                    [
                        (predicted, planned)
                        for predicted, planned
                        in zip(predicted_schedule, schedule)
                        if predicted != planned
                    ]
                )
            )
            assert sum(
                predicted - planned
                for predicted, planned
                in zip(schedule, predicted_schedule)
                if predicted - planned > 0
            ) <= limit_energy_production
            assert sum(
                power for power in schedule if power < 0
            ) >= limit_energy_consumption

            state = 0
            activations = 0
            for step in range(SCHEDULE_LENGTH):
                predicted = predicted_schedule[step]
                planned = schedule[step]

                if step == 0:
                    # First step
                    if predicted == planned:
                        # No activity at first time step
                        # So count as no activation
                        continue

                    # Activity at first time step
                    state = 1
                    # Count initial activity as activation
                    activations += 1

                # Non-first active step
                if predicted != planned:
                    if state:
                        # Continue activation
                        continue
                    else:
                        # Start activation
                        state = 1
                        # Do not count deactivations
                        # activations += 1

                # Non-first non-active step
                if state:
                    # Do not count deactivations
                    pass
                state = 0
                continue

            assert activations <= limit_count_activation
