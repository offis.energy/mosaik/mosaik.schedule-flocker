from random import randint

from mosaik_schedule_flocker.schedule_flocker.core.util import \
    get_activation_ends, SCHEDULE_LENGTH, get_filtered_activation_starts, \
    get_random_activation_starts


def test_get_activation_ends_closed_end():
    activation_starts = [1, 4, 23]
    activation_ends = get_activation_ends(
        activation_starts=activation_starts)

    assert activation_ends[0] in range(1, 4)
    assert activation_ends[1] in range(4, 24)
    assert activation_ends[2] == 23


def test_get_activation_ends_fussed():
    for _ in range(10):
        activation_starts = [randint(0, SCHEDULE_LENGTH) for _ in range(3)]
        activation_starts.sort()

        activation_starts = get_filtered_activation_starts(
            activation_starts=get_random_activation_starts(
                limit_count_activation=3)
        )
        print(f'activation_starts {activation_starts}')

        activation_ends = get_activation_ends(
            activation_starts=activation_starts,
        )

        assert len(activation_ends) >= 1
        assert len(activation_ends) <= len(activation_starts)

        assert activation_ends == sorted(activation_ends)
        assert all(
            end - start >= 0
            for end, start
            in zip(activation_ends, activation_starts)
        )

        assert max(activation_ends) < SCHEDULE_LENGTH
        assert min(activation_ends) >= 0


def test_get_activation_ends_duplicate():
    activation_starts = [1, 10, 11]
    activation_ends = get_activation_ends(
        activation_starts=activation_starts,
    )

    assert activation_ends[0] in range(1, 10)
    assert activation_ends[1] in range(10, 24)
    assert len(activation_ends) == 3


def test_get_activation_ends_open_end():
    # Mock
    activation_starts = [1, 4, 5]

    # Test
    activation_ends = get_activation_ends(
        activation_starts=activation_starts,
    )

    # Assert
    assert activation_ends[0] in range(1, 4)
    assert activation_ends[1] in range(4, 5)
    assert activation_ends[-1] in range(5, 23)
