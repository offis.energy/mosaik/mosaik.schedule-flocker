from mosaik_schedule_flocker.schedule_flocker.core.util import \
    get_random_activation_starts


def test_get_random_activation_starts():
    # Mock
    limit_count_activation = 4

    # Test
    random_activation_starts = get_random_activation_starts(
            limit_count_activation=limit_count_activation)

    # Assert
    assert random_activation_starts[0] in range(0, 24)
    assert random_activation_starts[1] in range(0, 24)
    assert random_activation_starts[2] in range(0, 24)
    assert random_activation_starts[3] in range(0, 24)
