from mosaik_schedule_flocker.schedule_flocker.core.util import \
    get_schedule_min


def test_get_schedule_min():
    # Mock
    predicted_flexibility_potential_negative = [1, 4, 1]
    predicted_schedule = [0, 5, 1]

    # Test
    schedule_min = get_schedule_min(
        predicted_flexibility_potential_negative=
        predicted_flexibility_potential_negative,
        predicted_schedule=predicted_schedule
    )

    # Assert
    assert schedule_min == [-1, 1, 0]
