from mosaik_schedule_flocker.schedule_flocker.core.util import \
    get_filtered_activation_starts


def test_get_filtered_activation_starts_short():
    # Mock
    activation_starts = [3]

    # Test
    activation_starts_filtered = get_filtered_activation_starts(
        activation_starts=activation_starts)

    # Assert
    assert activation_starts_filtered == [3]
