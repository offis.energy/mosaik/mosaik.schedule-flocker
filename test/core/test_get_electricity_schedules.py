from typing import List

from mosaik_schedule_flocker.schedule_flocker.model.electricity_schedule \
    import ElectricitySchedule
from mosaik_schedule_flocker.schedule_flocker.model.electricity_value import \
    ElectricityValue


def get_mock_electricity_schedules():
    electricity_schedules: List[ElectricitySchedule] = [
        ElectricitySchedule(
            electricity_values=[
                ElectricityValue(
                    startTime="2018-05-08T13:43:00+0100",
                    stepSize=900,
                    powerActive=3.14,
                    powerReactive=2.16,
                )
                for _ in range(10)
            ],
            id='my_unit_id',
            unit_type='my_unit_type',
            subnet=0,
        )
        for _ in range(18)
    ]

    # electricity_schedules = deepcopy_dict_schedules(electricity_schedules)

    return electricity_schedules


def test_get_electricity_schedules():
    # Test
    electricity_schedules = get_mock_electricity_schedules()

    # Assert
    assert electricity_schedules is not None
    assert isinstance(electricity_schedules, List)
    assert len(electricity_schedules) == 18

    for schedule in electricity_schedules:
        assert schedule is not None

        assert isinstance(schedule['id'], str)
        assert isinstance(schedule['unit_type'], str)
        assert isinstance(schedule['subnet'], int)
        assert isinstance(schedule['electricitySchedule'], List)

        for electricity_value in schedule['electricitySchedule']:
            assert electricity_value is not None

            assert isinstance(electricity_value['startTime'], str)
            assert isinstance(electricity_value['stepSize'], int)
            assert isinstance(electricity_value['powerActive'], float)
            assert isinstance(electricity_value['powerReactive'], float)

            print('electricity_value:', electricity_value)

        print('schedule:', schedule)
