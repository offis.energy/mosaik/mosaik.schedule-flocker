from mosaik_schedule_flocker.schedule_flocker.core.util import \
    get_activation_start_indices


def test_get_activation_start_indices():
    # Mock
    activation_starts = [1, 4, 5]

    # Test
    activation_start_indices = get_activation_start_indices(
            activation_starts=activation_starts)

    # Assert
    assert activation_start_indices == [0, 1, 2]
