from mosaik_schedule_flocker.schedule_flocker.core.util import get_schedule_max


def test_get_schedule_max():
    # Mock
    predicted_flexibility_potential_positive = [0, 1, 2]
    predicted_schedule = [0, 2, 4]

    # Test
    schedule_max = get_schedule_max(
        predicted_flexibility_potential_positive=
        predicted_flexibility_potential_positive,
        predicted_schedule=predicted_schedule
    )

    # Assert
    assert schedule_max == [0, 3, 6]
