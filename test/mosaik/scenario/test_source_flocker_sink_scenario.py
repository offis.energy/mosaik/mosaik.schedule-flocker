from mosaik_scenario_tools.scenario_tools.port.find_free_port_module import \
    find_free_port

from mosaik_schedule_flocker.schedule_flocker.mosaik.scenario.\
    source_flocker_sink_scenario_module import source_flocker_sink_scenario


def test_source_flocker_flocker_sink_scenario():
    # Mock
    mosaik_port = find_free_port()
    end = 1

    # Test
    source_flocker_sink_scenario(mosaik_port, end=end)

    # Assert
    # If the simulation terminates without an error, we consider it successful.
    assert True
