import mosaik
from mosaik_scenario_tools.scenario_tools.create.create_world_module import \
    create_world
from mosaik_scenario_tools.scenario_tools.port.find_free_port_module import \
    find_free_port
from mosaik_simconfig.simconfig.sim_config import \
    SimConfig
from mosaik_schedule_flocker.schedule_flocker.mosaik.source.\
    flocker_source_simulator import FlockerSourceSimulator
from mosaik_schedule_flocker.schedule_flocker.mosaik.create.\
    create_flocker_source_model_module import create_flocker_source_model


def test_create_flocker_source_model():
    # Mock
    sim_config = SimConfig()
    sim_config.add_in_process(simulator=FlockerSourceSimulator)
    world = create_world(
        sim_config=sim_config,
        mosaik_port=find_free_port(),
    )

    # Test
    model = create_flocker_source_model(world=world)

    # Assert
    assert model is not None
    assert isinstance(model, mosaik.scenario.Entity)
